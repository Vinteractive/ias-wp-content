<?php //Template Name: Contact Page ?>
<?php get_header(); ?>


<div class="fluid-container header">
		<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<h1><?php the_title(); ?></h1>

		<?php endwhile; endif; ?>
	</div>
</div>
<!-- / Header -->

<div class="container main-content">
	<div class="col-lg-6">
		
		<h2>CONTACT DETAILS</h2>
		<p class="address">
			Alliance House <br>
			12 Caxton Street <br> 
			London <br>
			SW1H 0QS
		</p>

		<p class="contact">
			<strong>Tel:</strong> 02072 224 001 <br>
			<strong>Email:</strong> <a href="mailto:info@alcoholresearchdirectory.com">info@alcoholresearchdirectory.com</a>
		</p>

		<h2>OUR LOCATION</h2>
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d39751.72778500348!2d-0.1467940010896863!3d51.486003729303604!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604deaf3a390d%3A0x657b344ae6813a12!2sAlliance+House%2C+12+Caxton+St%2C+Westminster%2C+London+SW1H+0QS!5e0!3m2!1sen!2suk!4v1432128439989" width="485" height="307" frameborder="0" style="border:0"></iframe>
	</div>
	<div class="col-lg-6">
		<?php echo do_shortcode('[gravityform id="2" title="true" description="false"]' ); ?>
	</div>
</div>



<?php get_footer(); ?>
