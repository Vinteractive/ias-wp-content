<?php // Template Name: Browse Research ?>
<?php get_header(); ?>


<div class="fluid-container header">
	<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>	

			SEARCH FORM	

		<?php endwhile; endif; ?>
	</div>
</div>

<div class="container main-content">
	<div class="row">
		
		<h1>ONGOING RESEARCH</h1>

		<table class="table table-bordered">
			<tr>
				<th>Type</th>
				<th>Title</th>
				<th>Summary / Abstract</th>
				<th>Keywords</th>
				<th>Type of Research</th>
				<th>Principle Investigator</th>
				<th>Completion Date</th>
			</tr>
		
		<?php $wp_query_ongoing = new WP_Query( array( 'post_type' => 'research', 'posts_per_page' => 6, 'order' => 'asc' ) ); ?>
		<?php while ( $wp_query_ongoing->have_posts() ) : $wp_query_ongoing->the_post(); //start of the loop ?>
					
		<?php
			$post_id = get_the_ID();
			$completion_date = get_post_meta( $post_id, "duration_end", true );
			$publication_date = get_post_meta( $post_id,  "publication_date", true );
		?>
			<?php// if (strtotime($publication_date) >= strtotime("now") || strtotime($completion_date) >= strtotime("now")) { ?>
			<?php if(($publication_date != '' && strtotime($publication_date) > time()) || ( $completion_date != '' && strtotime($completion_date) > time())){ ?>


			<tr>
				<td><?php echo do_shortcode('[wpuf-meta name="_hidden_type"]' ); ?></td>
				<td><?php the_title(); ?></td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="synopsis"]'); ?>
					<?php echo do_shortcode('[wpuf-meta name="abstract"]'); ?>
				</td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="keywords"]'); ?>
					

					<?php 
						$terms = get_the_terms($post->ID, 'keywords');  
						foreach ($terms as $keyword) {
							$myKeywords[] = $keyword->name;
						}
						echo implode( ', ', $myKeywords );
						$myKeywords = null;
					?>

				</td>
				<td><?php echo do_shortcode('[wpuf-meta name="type_of_research"]' ); ?></td>
				<td><?php echo do_shortcode('[wpuf-meta name="principle_investigator"]' ); ?></td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="duration_end"]'); ?>
					<?php echo do_shortcode('[wpuf-meta name="publication_date"]'); ?>
				</td>
			</tr>
		
			<?php } ?>
		
		<?php endwhile; //end of the loop ?>
		
		</table>
		

		<h1>PUBLISHED RESEARCH</h1>
			
		<table class="table table-bordered">
			<tr>
				<th>Type</th>
				<th>Title</th>
				<th>Summary / Abstract</th>
				<th>Keywords</th>
				<th>Author</th>
				<th>Type of Research</th>
				<th>Open Access</th>
				<th>Date Published</th>
			</tr>

		<?php $wp_query_published = new WP_Query( array( 'post_type' => 'research', 'posts_per_page' => 6, 'order' => 'asc' ) ); ?>
		<?php while ( $wp_query_published->have_posts() ) : $wp_query_published->the_post(); //start of the loop ?>
					
		<?php
			$post_id = get_the_ID();
			$completion_date = get_post_meta( $post_id, "duration_end", true );
			$publication_date = get_post_meta( $post_id,  "publication_date", true );
		?>

			<?php // if ( strtotime($publication_date) <= strtotime("now") || strtotime($completion_date) <= strtotime("now")){ ?>
			<?php if(($publication_date != '' && strtotime($publication_date) <= time()) || ( $completion_date != '' && strtotime($completion_date) <= time())){ ?>

			<tr>
				<td><?php echo do_shortcode('[wpuf-meta name="_hidden_type"]' ); ?></td>
				<td><?php the_title(); ?></td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="synopsis"]'); ?>
					<?php echo do_shortcode('[wpuf-meta name="abstract"]'); ?>
				</td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="keywords"]'); ?>
					

					<?php 
						$terms = get_the_terms($post->ID, 'keywords');  
						foreach ($terms as $keyword) {
							$myKeywords[] = $keyword->name;
						}
						echo implode( ', ', $myKeywords );
						$myKeywords = null;
					?>

				</td>
				<td><?php echo do_shortcode('[wpuf-meta name="author"]' ); ?></td>
				<td><?php echo do_shortcode('[wpuf-meta name="type_of_research"]'); ?></td>
				<td><?php echo do_shortcode('[wpuf-meta name="open_access"]'); ?></td>
				<td>
					<?php echo do_shortcode('[wpuf-meta name="duration_end"]'); ?>
					<?php echo do_shortcode('[wpuf-meta name="publication_date"]'); ?>
				</td>
			</tr>		
			
			<?php } ?>

		<?php endwhile; //end of the loop ?>

		</table>
	</div>
</div>

<?php get_footer(); ?>