<?php // Template Name: Home Page ?>

<?php get_header(); ?>

<div class="fluid-container home-banner">
	<div class="container">
		
		<div class="col-lg-6">
			<div class="search-directory">	
				<h2>Search the Directory</h2>
				
				<div class="search-directory-wrapper">


				<span>Search by:</span>
			    	<div class="form-group">
			      		<form action="<?php echo get_permalink('209' ); ?>">
			      			<label for="">People</label>
			      			<input type="search" value="" name="swpquery" id="searchDirectory-people"><br>
			      			<label for="">Keyword</label>
			      			<input type="search" value="" name="swpquery" id="searchDirectory-keyword"><br>
			      			<label for="">Institution</label>
			      			<input type="search" value="" name="swpquery" id="searchDirectory-Institution">
			      		
					</div>
				</div>

					<a href="">Advanced Search</a>

					<input type="submit" id="search-directory-submit" value="Search">

				</form>

			</div>
		</div>

		<div class="col-lg-6">
			<a href="">
				<div class="browse-projects">
					<h2>Browse Ongoing Projects</h2>
				</div>
			</a>
			
			<a href="">
				<div class="available-data">
					<h2>Available Data Sets</h2>
				</div>
			</a>
		</div>

	</div>
</div>


<div class="container main-content">

	<div class="col-lg-12">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; endif; ?>
	</div>

</div>

<?php get_footer(); ?>