<?php // Template Name: Upload Research ?>
<?php get_header(); ?>

<div class="fluid-container header">
		<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="col-lg-7">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="col-lg-5">

				<ul class="wpuf-form upload-type-selector pull-right">
					<li class="wpuf-el type text-right">       
						<div class="wpuf-label">
				            <label for="wpuf-_hidden_type">Type <span class="required">*</span></label>
			        	</div>
				        <div class="wpuf-fields">
				            <select class="wpuf_type" name="type" data-required="yes" data-type="select">
                                <option value="" selected="selected">Please select...</option>
                                <option value="Project">Project</option>
                                <option value="Paper">Paper</option>
                            </select>
				            <!-- <span class="wpuf-help"></span> -->
				        </div>
			        </li>
				</ul>
			</div>
	</div>
</div>
<!-- / Header -->

<div class="container main-content">
	<div class="col lg-12">
		
		<div class="please-select">
			<h2>Please select a type from the above dropdown</h2>
		</div>
		
		<div class="project upload-form">
			<?php echo do_shortcode('[wpuf_form id="98"]' ); ?>
		</div>

		<div class="paper upload-form">
			<?php echo do_shortcode('[wpuf_form id="128"]'); ?>
		</div>

	</div>
</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
