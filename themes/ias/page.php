<?php get_header(); ?>

<div class="fluid-container header">
		<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h1><?php the_title(); ?></h1>
	</div>
</div>
<!-- / Header -->

<div class="container main-content">
	<div class="col lg-12">
		<?php the_content(); ?>
	</div>
</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>