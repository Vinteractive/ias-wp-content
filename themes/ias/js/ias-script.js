jQuery(document).ready(function($) {

	// Dynamic resize of search bar (home)
    // var bar = $('#s').width();
    // bar = bar - 150; // width of label
    // $('#s').css("width", bar );

    // Upload Research Form Select		
	$('.project.upload-form').hide();
    $('.paper.upload-form').hide();

    $('.wpuf_type').change( function() {
    	var $type = $('.wpuf_type').find(":selected").text().toLowerCase();
    	$('.project.upload-form').hide();
    	$('.paper.upload-form').hide();
 		$('.please-select').hide();
 	   	
 	   	if ($type === "please select...") {
    		$('.please-select').fadeIn();
    	} else {
    		$('.' + $type).fadeIn();
    	}
 
    });

    // Contact Form Restructure
    $('div.gform_heading').prependTo('#gform_wrapper_2');

    // $('div.gform_footer.left_label').appendTo('#gform_wrapper_2');


});