<?php // Template Name: Browse Academics and Researchers ?>
<?php get_header(); ?>


<div class="fluid-container header">
		<div class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<h1><?php the_title(); ?></h1>
	</div>
</div>
<!-- / Header -->

<?php
// $args = array(
// 	.
// 	.
// 	.
// );

$args = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => '',
	'meta_key'     => '',
	'meta_value'   => '',
	'meta_compare' => '',
	'meta_query'   => array(),
	'include'      => array(),
	'exclude'      => array(),
	'orderby'      => 'login',
	'order'        => 'ASC',
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => ''
	);

// The Query
$user_query = new WP_User_Query( $args );

print_r($user_query);

// User Loop
if ( ! empty( $user_query->results ) ) {
	foreach ( $user_query->results as $user ) {
		echo '<p>' . $user->display_name . '</p>';
	}
} else {
	echo 'No users found.';
}
?>

<?php endwhile; endif; ?>
<?php get_footer(); ?>