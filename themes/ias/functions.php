<?php 	

// Scripts + Styles
function enqueue_styles_scripts() {
	wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/css/style.css');
	// wp_enqueue_script( 'scripts', get_stylesheet_directory_uri() . '/js/min/script-min.js');

	
	wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/js/min/jquery-min.js');
	wp_enqueue_script( 'jquery-ui', get_stylesheet_directory_uri() . '/js/min/jquery-ui-min.js');
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/min/bootstrap-min.js');
	wp_enqueue_script( 'scripts', get_stylesheet_directory_uri() . '/js/ias-script.js');
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_scripts' );

// Menus
function register_menus() {
	register_nav_menu('loggeg-in-menu',__( 'Logged IN Menu' ));
	register_nav_menu('loggeg-out-menu',__( 'Logged OUT Menu' ));
	register_nav_menu('footer-menu', 'Footer Menu' );
}
add_action( 'init', 'register_menus' );

//excerpt length
function custom_excerpt_length( $length ) {
	return 52;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


// options pages:
// acf_add_options_sub_page(array(
//     'title' => 'Notice Board Options',
//     'parent' => 'edit.php?post_type=notice_board',
//     'capability' => 'manage_options'
// ));


// add_action( 'after_setup_theme', 'theme_setup' );
// function theme_setup() {
//   add_image_size( 'notice-board-thumb', 155, 96 ); 
// }


?>