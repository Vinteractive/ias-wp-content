<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />

		<title><?php wp_title( ' | ', true, 'right' ); ?></title>

		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>

<header>
	<div class="fluid-container">
		<div class="container">

		<div class="col-lg-12">
			
			<div class="logo">
				<a href="/">
					<h1>The Alcohol <br>
						Research Directory </h1>
				</a>
			</div>


			<div id="search">
				<?php echo get_search_form( ); ?>
			</div>
			
	
			<!-- Search -->
			<div id="search">
				<!-- <form role="search" method="get" id="searchform" class="form-horizontal searchform" action="http://ias.dev/">
					<div>
						<label class="screen-reader-text" for="s">Search Directory:</label>
						<input type="text" value="" name="s" id="s">
						<input type="submit" id="searchsubmit" value="Search">
					</div>
					
				</form> -->
			</div>
			<!-- /search -->

			<div class="tweet">
				<a href="">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/img/tweet-page.png" alt="">
				</a>
			</div>
			
			<!-- Logged Out -->
			<!-- <nav class="logged-out">
				<?php 
					wp_nav_menu( array(
						'menu' 			=> 	'Logged OUT Menu'
				  	)); 
				?>
			</nav> -->
			<!-- / Logged Out -->
			

			<nav class="logged-in">
				<ul class="nav nav-pills">
					<li role="presentation" class="dropdown">
				    	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
				      		My Account
				    	</a>
				    	<?php 
							wp_nav_menu( array(
								'menu'				=>	'Logged IN Menu',
								'container'			=>	false,
								'menu_class'		=> 	'dropdown-menu'
							));
						?>
				  	</li>
				</ul> 
			</nav>
		
			</div>
			<!-- / end col 12 -->

		</div>
	</div>
</header>