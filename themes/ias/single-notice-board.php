<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 

	// Custom content splitting 
	$content = apply_filters( 'the_content', get_the_content() );
	$content = explode (" ", $content); // convert to an array of words
	$word_num = count($content); //get the number of words;

	// get first 52
	for ($i = 0; $i <= 51; $i++) {
	    $the_excerpt .= $content[$i] . " ";
	}

	// get the rest
	for ($i = 52; $i <= $word_num; $i++) {
	    $the_content .= $content[$i] . " ";
	}

?>


<div class="fluid-container header">
	<div class="container">
		<div class="row col-lg-12">
		
			<div class="col-lg-8">
				<a class="back" href="/notice-board"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/back-arrow-blue.png" width="10" height="10"> Back to Notice Board</a>
				
				<h1><?php the_title(); ?></h1>
					<?php echo $the_excerpt; ?>...
			</div>

			<div class="col-lg-4">
				<?php the_post_thumbnail(); ?>
			</div>

		</div>
	</div>	
</div>

<div class="container main-content">
	<div class="col-lg-12">
		...<?php echo $the_content; ?>
	</div>
</div>

<?php endwhile; endif; ?>	

<?php get_footer(); ?>