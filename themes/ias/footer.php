<footer>
	<div class="container">
		<div class="col-lg-5">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer-logos.png" width="279" height="32">
		</div>

		<div class="col-lg-7">
				
			<nav>
				<?php 
					wp_nav_menu( array(
						'menu'			=> 	'Footer Menu',
						'menu_class'	=>	'list-inline'
					));
				?>
			</nav>

			<p class="copyright">
				Copyright &copy; 2015 Institute of Alcohol Studies and Alcohol Research UK
			</p>

		</div>

	</div>
</footer>
<?php wp_footer(); ?>