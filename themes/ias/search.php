<?php get_header(); ?>

<div class="fluid-container header">
	<div class="container">
		<?php if ( have_posts() ) : ?>
		<h1 class="entry-title"><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h1>
	</div>
</div>

<div class="container main-content">
<div class="col-lg-12">
	<?php while ( have_posts() ) : the_post(); ?>
		<article class="row">
			<h1><?php the_title(); ?></h1>
			<div class="col-lg-8">
				<strong>Type:</strong> <?php echo do_shortcode('[wpuf-meta name="_hidden_type"]' ); ?> <span class="seperator">|</span> 
				<strong>Keywords:</strong> <?php echo do_shortcode('[wpuf-meta name="keywords"]'); ?>

				<?php 
					$terms = get_the_terms($post->ID, 'keywords');  
					foreach ($terms as $keyword) {
						$myKeywords[] = $keyword->name;
					}
					echo implode( ', ', $myKeywords );
					$myKeywords = null;
				?>
				<span class="seperator">|</span> 
				<strong>Principle Investigator / Author:</strong>
				<?php echo do_shortcode('[wpuf-meta name="principle_investigator"]' ); ?>
				<?php echo do_shortcode('[wpuf-meta name="author"]' ); ?>

			</div>
			<div class="col-lg-4">
			<strong>Synopsis / Abstract: </strong> <br>
				<?php echo do_shortcode('[wpuf-meta name="synopsis"]'); ?>
				<?php echo do_shortcode('[wpuf-meta name="abstract"]'); ?>
			</div>
		</article>
		
		<hr>
		
		<?php endwhile; //end of the loop ?>
	
	<?php else : ?>
	
		<h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
		<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
			<?php get_search_form(); ?>
	<?php endif; ?>
</div>
</div>


<?php get_footer(); ?>