<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="fluid-container header">
	<div class="container">
		<h1><?php the_title(); ?></h1>	
	</div>

</div>

<div class="container">
	<?php the_content(); ?>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>