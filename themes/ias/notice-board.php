<?php 

// Template Name: Notice Board 

// USED AS AN ARCHIVE
?>

<?php get_header(); ?>

<div class="fluid-container header">
	<div class="container">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div class="col-lg-10">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="col-lg-2">
			<a href="/post-new-notice" class="button pull-right">Post New Notice</a>
		</div>
		<div class="col-lg-12">
			<p><?php the_content(); ?></p>
		</div>

		<?php endwhile; endif; ?>

	</div>
</div>	

<div class="container main-content">

	<div class="col-lg-12 ">

	<?php $loop = new WP_Query( array( 'post_type' => 'notice_board', 'posts_per_page' => 9, 'order' => 'asc' ) ); ?>

	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
		
		<article class="row">
			<div class="col-lg-10">
				<a href="<?php the_permalink(); ?>">
					<h1><?php the_title(); ?></h1>
				</a>
				<p><?php the_excerpt(); ?></p>
			</div>
			<div class="col-lg-2">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'medium' ); ?>
				</a>
			</div>
		</article>
		
		<div class="clearfix"></div>
		<hr>

	<?php endwhile; ?>

	</div>	

</div>


<?php get_footer(); ?>